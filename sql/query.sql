SELECT app_id,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE device_id = a.device_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id) AS rule_1,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE device_id = a.device_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id
           AND rejected = 1) AS rule_2,
       (SELECT count(DISTINCT person_id)
          FROM am_data
         WHERE device_id = a.device_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id) AS rule_3,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE ip_address_id = a.ip_address_id
           AND sent_at < a.sent_at
           AND sent_at >= datetime(a.sent_at, '-14 days')
           AND customer_id != a.customer_id
           AND rejected = 1) AS rule_4,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE contact_phone_id = a.mobile_phone_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id
           AND rejected = 1) AS rule_5,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE work_phone_id = a.mobile_phone_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id
           AND rejected = 1) AS rule_6,
       (SELECT count(DISTINCT customer_id)
          FROM am_data
         WHERE person_id = a.person_id
           AND passport_id != a.passport_id
           AND sent_at < a.sent_at
           AND customer_id != a.customer_id) AS rule_7
  FROM am_data AS a;
